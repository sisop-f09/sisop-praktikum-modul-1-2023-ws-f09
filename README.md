## Anggota Kelompok

Abyan Zhafran Trisnanto // 5025211218

Fauzi Rizki Pratama // 5025211220

Khairuddin Nasty // 5025201041

Rani Listian Anggraeni // 5025211171

### Soal Shift
- [link](https://drive.google.com/drive/folders/1Zof7rySo9QdzmAhfNc1vFMXnIbLM-SRD)

### Soal 1
    Terdapat 4 perintah pada soal pertama
    1. Menampilkan 5 rank teratas universitas di Jepang
        untuk memfilter data diperlukan command 'awk' dengan keyword 'JP'. Kemudian karena hanya 5 data
        menggunakan 'head -5'
    2. menampilkan fsr terendah dari hasil di atas
        maka, command diatas ditambahkan command 'sort' bedasarkan kolom fsr. Kemudian diambil satu saja
        dengan command 'head -1'
    3. Menampilkan 10 universitas teratas bedasarkan ger rank
        Maka, sama dengan perintah pertama, hanya saja dilakukan 'sort' terlebih dahulu, kemudian 'head-10'
    4. Menampilkan universitas dengan kata kunci 'keren'
        sama dengan perintah pertama, hanya mengganti 'JP' menjadi 'Keren'.
### Soal 2

### Soal 3
#### louis.sh 
```shell
#!/bin/bash

# Fungsi mengecek kompleksitas password
checkPasswordComplexity() {
    PASSWORD="$1"
    USERNAME="$2"

    # mengecek panjang password
    if [[ ${#PASSWORD} -lt 8 ]]; then
        echo "Password length should be at least 8 characters" >&2
        exit 1
    fi

    # mengecek huruf besar dan huruf kecil
    if [[ ! "$PASSWORD" =~ [[:upper:]] || ! "$PASSWORD" =~ [[:lower:]] ]]; then
        echo "Password should contain both upper and lower case letters" >&2
        exit 1
    fi

    # mengecek apakah password alphanumeric
    if [[ ! "$PASSWORD" =~ ^[[:alnum:]]+$ ]]; then
        echo "Password should be alphanumeric" >&2
        exit 1
    fi

    # mengecek apakah password sama dengan username
    if [[ "$PASSWORD" == "$USERNAME" ]]; then
        echo "Password should not be the same as username" >&2
        exit 1
    fi

    # mengecek apakah password mengandung kata chicken atau ernie
    if [[ "$PASSWORD" =~ chicken || "$PASSWORD" =~ ernie ]]; then
        echo "Password should not contain 'chicken' or 'ernie'" >&2
        exit 1
    fi
}

# mendapatkan input username dan password
read -p "Enter username: " user
read -p "Enter password: " -s passwd
echo

# mengecek apakah user sudah ada
if grep -q "^$user:" /users/users.txt; then
    echo "User already exists" >&2
    echo "$(date +%Y/%m/%d\ %H:%M:%S) REGISTER: ERROR User already exists" >> log.txt
    exit 1
fi

# mengecek kompleksitas password
checkPasswordComplexity "$passwd" "$user"

# menambahkan username dan password ke file users.txt
echo "$user:$passwd" >> /users/users.txt
echo "$(date +%Y/%m/%d\ %H:%M:%S) REGISTER: INFO User $user registered successfully" >> log.txt
```
##### Penjelasan
1. Membuat fungsi `checkPasswordComplexity` dengan parameter pertama `PASSWORD` dan parameter kedua `USERNAME` untuk mengecek kompleksitas password. Jika password yang dimasukkan tidak sesuai dengan ketentuan, maka sistem akan menampilkan pesan error sesuai dengan kesalahan yang dilakukan.
2. Menerima input dari user berupa username dan password dan memasukkannya kedalam variabel `user` dan `passwd`
3. Mengecek apakah username yang diinputkan sudah terdaftar pada `/users/users.txt`, jika iya, maka sistem akan menampilkan pesan `"User already exists"` dan pada log akan tercatat `REGISTER: ERROR User
already exists`.
4. Mengecek kompleksitas password dengan memanggil fungsi `checkPasswordComplexity` 
5. Jika register berhasil, data username dan password yang diinputkan akan tercatat di `/users/users.txt` dan pada log akan tercatat `REGISTER: INFO User $user registered successfully` dengan `$user` sebagai username 

#### retep.sh
```shell
#!/bin/bash

# input username dan password
echo "Please enter your username:"
read user
echo "Please enter your password:"
read -s passwd

# mengecek apakah user terdaftar pada file users.txt
if ! grep -q "^$user:" /users/users.txt; then
    echo "User does not exist"
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $user" >> log.txt
    exit 1
fi

# mengambil password dari file users.txt
userpasswd=$(grep "^$user:" /users/users.txt | cut -d ":" -f 2)

# mengecek apakah password sudah benar
if [ "$userpasswd" != "$passwd" ]; then
    echo "Incorrect password."
    echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $user" >> log.txt
    exit 1
fi

echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $user logged in" >> log.txt
echo "Welcome, $user!"
exit 0
```
##### Penjelasan
1. Menerima input username dan password dari user dan dimasukkan ke dalam variabel `user` dan `passwd`
2. Mengecek apakah user yang diinputkan sudah terdaftar pada `users.txt` menggunakan `grep`, jika tidak ada, maka sistem akan menampilkan pesan error `"User does not exist"` dan pada log akan tercatat `LOGIN: ERROR Failed login attempt on user $user`
3. Mengambil data password dari `users.txt` sesuai dengan username yang diinputkan, kemudian dicocokkan dengan password yang diinputkan. Jika tidak cocok, tampilkan pesan `"Incorrect password."` dan pada log akan tercatat `LOGIN: ERROR Failed login attempt on user $user`.
4. Jika username dan password sudah sesuai dengan yang ada pada `users.txt`, maka sistem akan menampilkan pesan `"Welcome, $user!"`, dan pada log akan tercatat `LOGIN: INFO User $user logged in` dengan `$user` sebagai username.
### Soal 4

#### Penjelasan
Terdapat 4 perintah pada soal ke empat

1. Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).
2. Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
3. Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14. Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
Setelah huruf z akan kembali ke huruf a
4. Buat juga script untuk dekripsinya.
Backup file syslog setiap 2 jam untuk dikumpulkan 😀.
#### Log_encrypt.sh

```shell
#/!bin/bash


# nama file hasil enkripsi, 
# disini saya menggunakan  nama dengan namafile
filename=$(date "+%H":"%M %d":"%m":"%y")

# variabel untuk mendapatkan jam sekarang dan dimasukkan ke  variabel hour 
hour=$(date "+%H")

# buat array untuk mendapatkan value per hurufnya

#saya menggunakan nama variabel alfabet untuk menyimpan array berisi huruf kecil
alfabet=(a b c d e f g h i j k l m n o p q r s t u v w x y z)


#saya menggunakan nama variabel kapital untuk menyimpan array berisi huruf besar
kapital=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# sandi 1 merupakan string untuk parameter tr yang digunakan untuk menggeser baris huruf alfabet
sandi1="${alfabet[$hour]}-z}a-${alfabet[$hour-1]}"

# sandi 2 merupakan string untuk parameter rt yang digunakan untuk menggeser baris huruf kapital
sandi2="${kapital[$hour]}-Z}A-${kapital[$hour-1]}"

# simpan hasil enkripsi ke file yang diinginkan
cd '/home/fauzi/sisop/mod1-s4'

#  menyimpan hasil enrktipsi dari string
echo -n "$(cat /var/log/syslog | tr 'a-z' $sandi1 | tr 'A-Z' $sandi2)" > "$filename.txt"

# 0 */2 * * * /bin/bash /home/fauzi/sisop/mod1-s4/log_encrypt.sh
```
#### Penjelasan
1. input berupa syslog. output ke file dengan format "HH:MM dd:mm:yy.txt".
2. Selanjutnya ambil key dari jam dimana script dijalankan. 
3. Buat cipher untuk upper case dan lower case sesuai key. 
4. Kemudian tampilkan input lalu transform 'a-z' ke cipher lower lalu transform lagi 'A-Z' ke cipher upper. 
5. Dan yang terakhir outputkan hasil dari enkripsi tersebut ke file yang diinginkan

Hasil dari file yang di enkripsi:
```shell
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@zdnkiako_lopkna.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha zdnkiako_lopkna.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@zkjbecbo.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha zkjbecbo.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@}ni.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha }ni.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@abe_lopkna.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha abe_lopkna.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@bqoa.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}-ik}qhao-hkx}[241]: Ejoanpa} ik}qha 'hl'
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha bqoa.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@lopkna_yhg.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha lopkna_yhg.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@lopkna_vkja.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha lopkna_vkja.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@nxikklo.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha nxikklo.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Naikqjp Nkkp xj} Ganjah Beha Ouopaio.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}-ik}qhao-hkx}[241]: Ejoanpa} ik}qha 'll}ar'
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Xzperxpejc osxl /osxlbeha...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}-ik}qhao-hkx}[241]: Ejoanpa} ik}qha 'lxnlknp_lz'
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Ikqjpejc BQOA Zkjpnkh Beha Ouopai...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Ikqjpejc Ganjah Zkjbecqnxpekj Beha Ouopai...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}-ik}qhao-hkx}[241]: Ejoanpa} ik}qha 'ion'
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}-ik}qhao-hkx}[241]: Ik}qha 'bqoa' eo yqehp ej
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Opxnpejc Bhqod Fkqnjxh pk Lanoeopajp Opknxca...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Zkj}epekj zdazg naoqhpa} ej Lhxpbkni Lanoeopajp Opknxca Xnzderxh yaejc ogella}.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Opxnpejc Hkx}/Oxra Nxj}ki Oaa}...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Opxnpejc Znaxpa Ouopai Qoano...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Xzperxpa} osxl /osxlbeha.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Oap pda zkjokha gauykxn} hxukqp.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Ikqjpa} BQOA Zkjpnkh Beha Ouopai.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Ikqjpa} Ganjah Zkjbecqnxpekj Beha Ouopai.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Naxzda} pxncap Osxlo.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Bhqod Fkqnjxh pk Lanoeopajp Opknxca.
```

Berikut output yang dihasilkan dari terminal:
``` Shell
log_encrypt.sh: line 30: warning: command substitution: ignored null byte in input
``` 
#### Log_decrypt.sh

``` Shell
#/!bin/bash

# buat file dekripsi dengan format nama file
# disini saya menggunakan format namafie

namafile=$(date "+%H":"%M %d":"%m":"%y decrypt")

# kemudian simpan waktu atau jam sekarang pada variabel hour
hour=$(date "+%H")

# buat array untuk menyimpan huruf agar bisa didapat valuenya

#saya menggunakan nama variabel alfabet untuk menyimpan array dengan huruf kec>
alfabet=(a b c d e f g h i j k l m n o p q r s t u v w x y z )

#saya menggunakan nama variabel kapital untuk menyimpan array yang berisi huru>
kapital=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)

# sandi 1 merupakan string untuk parameter tr yang digunakan untuk menggeser b>
sandi1="${alfabet[$hour]}-z}a-${alfabet[$hour-1]}"
# dan sandi 2 (kapital) adalah string untuk parameter tr yang menggeser barisa>
sandi2="${kapital[$hour]}-Z}A-${kapital[$hour-1]}"

# pindah ke directory yang diinginkan untuk menyimpan hasil dekripsi
cd '/home/fauzi/sisop/mod1-s4'
```

Penjelasan
1. Input yang digunakan adalah berupa syslog yang telah dienkripsi kemudian akan di output ke file dengan format "HH:MM dd:mm:yy.txt". 
2. Kemudian ambil encryptedKey dari jam yang ada di nama file input. 
3. Kemudian akan dilakukan operasi key dimana key = 26 - encryptedKey untuk membalikkan dari hasil enkripsi tadi. 
4. Lalu buat cipher untuk upper case dan lower case sesuai key tampilkan input lalu transform 'a-z' ke cipher lower lalu transform lagi 'A-Z' ke cipher upper. 
5. Yang terakhir hasil file yang sudah ke dekripsi tadi di outputkan ke file yang diinginkan.

Input yang digunakan pada log_backup:
```shell
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@zdnkiako_lopkna.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha zdnkiako_lopkna.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@zkjbecbo.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha zkjbecbo.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@}ni.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha }ni.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@abe_lopkna.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha abe_lopkna.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@bqoa.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}-ik}qhao-hkx}[241]: Ejoanpa} ik}qha 'hl'
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha bqoa.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@lopkna_yhg.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha lopkna_yhg.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@lopkna_vkja.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha lopkna_vkja.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: ik}lnkya@nxikklo.oanreza: }axzperxpa} oqzzaoobqhhu.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Hkx} Ganjah Ik}qha nxikklo.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Naikqjp Nkkp xj} Ganjah Beha Ouopaio.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}-ik}qhao-hkx}[241]: Ejoanpa} ik}qha 'll}ar'
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Xzperxpejc osxl /osxlbeha...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}-ik}qhao-hkx}[241]: Ejoanpa} ik}qha 'lxnlknp_lz'
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Ikqjpejc BQOA Zkjpnkh Beha Ouopai...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Ikqjpejc Ganjah Zkjbecqnxpekj Beha Ouopai...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}-ik}qhao-hkx}[241]: Ejoanpa} ik}qha 'ion'
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}-ik}qhao-hkx}[241]: Ik}qha 'bqoa' eo yqehp ej
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Opxnpejc Bhqod Fkqnjxh pk Lanoeopajp Opknxca...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Zkj}epekj zdazg naoqhpa} ej Lhxpbkni Lanoeopajp Opknxca Xnzderxh yaejc ogella}.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Opxnpejc Hkx}/Oxra Nxj}ki Oaa}...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Opxnpejc Znaxpa Ouopai Qoano...
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Xzperxpa} osxl /osxlbeha.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Oap pda zkjokha gauykxn} hxukqp.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Ikqjpa} BQOA Zkjpnkh Beha Ouopai.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Ikqjpa} Ganjah Zkjbecqnxpekj Beha Ouopai.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Naxzda} pxncap Osxlo.
Ixn  5 14:46:21 bxqve-RenpqxhYkt ouopai}[1]: Bejeoda} Bhqod Fkqnjxh pk Lanoeopajp Opknxca.
```
Output file yang dihasilkan:
``` Shell
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: modprobe@chromeos_pstore.service: deactivated successfully.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Finished Load Kernel Module chromeos_pstore.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: modprobe@configfs.service: deactivated successfully.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Finished Load Kernel Module configfs.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: modprobe@drm.service: deactivated successfully.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Finished Load Kernel Module drm.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: modprobe@efi_pstore.service: deactivated successfully.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Finished Load Kernel Module efi_pstore.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: modprobe@fuse.service: deactivated successfully.
Mar  5 14:46:21 fauzi-VirtualBox systemd-modules-load[241]: Inserted module 'lp'
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Finished Load Kernel Module fuse.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: modprobe@pstore_blk.service: deactivated successfully.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Finished Load Kernel Module pstore_blk.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: modprobe@pstore_zone.service: deactivated successfully.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Finished Load Kernel Module pstore_zone.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: modprobe@ramoops.service: deactivated successfully.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Finished Load Kernel Module ramoops.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Finished Remount Root and Kernel File Systems.
Mar  5 14:46:21 fauzi-VirtualBox systemd-modules-load[241]: Inserted module 'ppdev'
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Activating swap /swapfile...
Mar  5 14:46:21 fauzi-VirtualBox systemd-modules-load[241]: Inserted module 'parport_pc'
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Mounting FUSE Control File System...
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Mounting Kernel Configuration File System...
Mar  5 14:46:21 fauzi-VirtualBox systemd-modules-load[241]: Inserted module 'msr'
Mar  5 14:46:21 fauzi-VirtualBox systemd-modules-load[241]: Module 'fuse' is built in
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Starting Flush Journal to Persistent Storage...
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Condition check resulted in Platform Persistent Storage Archival being skipped.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Starting Load/Save Random Seed...
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Starting Create System Users...
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Activated swap /swapfile.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Finished Set the console keyboard layout.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Mounted FUSE Control File System.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Mounted Kernel Configuration File System.
Mar  5 14:46:21 fauzi-VirtualBox systemd[1]: Reached target Swaps.
```
#Kendala dan error

kendala dan error saat mengerjakan soal 4 adalah terdapat error di terminal: 
``` Shell
log_encrypt.sh: line 30: warning: command substitution: ignored null byte in input
``` 
Namun, script bisa dijalankan dan bisa digunakan dengan baik 

### REVISI 

#### Soal 4
Pada file log_backup.sh sendiri tidak bisa ter dekripsi dengan baik dikarenakan ada kesalahan pada satu line di codingannya, namun sudah melakukan pembetulan dann fungsi bisa dijalankan dengan baik



