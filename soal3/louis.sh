#!/bin/bash

# Fungsi mengecek kompleksitas password
checkPasswordComplexity() {
    PASSWORD="$1"
    USERNAME="$2"

    # mengecek panjang password
    if [[ ${#PASSWORD} -lt 8 ]]; then
        echo "Password length should be at least 8 characters" >&2
        exit 1
    fi

    # mengecek huruf besar dan huruf kecil
    if [[ ! "$PASSWORD" =~ [[:upper:]] || ! "$PASSWORD" =~ [[:lower:]] ]]; then
        echo "Password should contain both upper and lower case letters" >&2
        exit 1
    fi

    # mengecek apakah password alphanumeric
    if [[ ! "$PASSWORD" =~ ^[[:alnum:]]+$ ]]; then
        echo "Password should be alphanumeric" >&2
        exit 1
    fi

    # mengecek apakah password sama dengan username
    if [[ "$PASSWORD" == "$USERNAME" ]]; then
        echo "Password should not be the same as username" >&2
        exit 1
    fi

    # mengecek apakah password mengandung kata chicken atau ernie
    if [[ "$PASSWORD" =~ chicken || "$PASSWORD" =~ ernie ]]; then
        echo "Password should not contain 'chicken' or 'ernie'" >&2
        exit 1
    fi
}

# mendapatkan input username dan password
read -p "Enter username: " user
read -p "Enter password: " -s passwd
echo

# mengecek apakah user sudah ada
if grep -q "^$user:" /users/users.txt; then
    echo "User already exists" >&2
    echo "$(date +%Y/%m/%d\ %H:%M:%S) REGISTER: ERROR User already exists" >> log.txt
    exit 1
fi

# mengecek kompleksitas password
checkPasswordComplexity "$passwd" "$user"

# menambahkan username dan password ke file users.txt
echo "$user:$passwd" >> /users/users.txt
echo "$(date +%Y/%m/%d\ %H:%M:%S) REGISTER: INFO User $user registered successfully" >> log.txt
